Option Explicit On
Option Strict On
Module util
    Public Const connStr As String = "Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;Data Source=./dnd.mdb"
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" _
            (ByVal lpApplicationName As String, ByVal lpKeyName As String, _
             ByVal lpDefault As String, ByVal lpReturnedString As String, _
             ByVal nSize As Long, ByVal lpFileName As String) As Long

    Public Sub extClearForm(ByVal container As Control)
        Dim ctrl As Control
        For Each ctrl In container.Controls

            If TypeOf ctrl Is TextBox Then
                ctrl.Text = ""
            ElseIf TypeOf ctrl Is ListBox Then
                CType(ctrl, ListBox).Items.Clear()
            ElseIf TypeOf ctrl Is CheckBox Then
                CType(ctrl, CheckBox).Checked = False
            End If
            If ctrl.HasChildren Then
                extClearForm(ctrl)
            End If
        Next
    End Sub

    Public Function CNum(ByVal newParam As Object, Optional ByVal defaultValue As Double = 0) As Double
        Dim temp As Double
        Try
            temp = CDbl(newParam.ToString)
        Catch ex As System.InvalidCastException
            temp = defaultValue
        End Try
        Return temp
    End Function
    Public Function CNumI(ByVal newParam As Object, Optional ByVal defaultValue As Integer = 0) As Integer
        Dim temp As Integer
        Try
            temp = CInt(newParam.ToString)
        Catch ex As System.InvalidCastException
            temp = defaultValue
        End Try
        Return temp
    End Function

    Public Function FormatN(ByVal Texto As Object, Optional ByVal Ceros As Integer = 2) As String
        If Ceros < 0 Then Ceros = 0
        FormatN = Format(CNum(Texto), "###,###,##0" & IIf(Ceros > 0, ".", "").ToString & New String(CChar("0"), Ceros))
    End Function

    Public Sub Salto(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            e.Handled = True 'equivalente al keyascii=0
            SendKeys.Send("{tab}")
        End If
    End Sub

    Public Function Dise�o() As Boolean
        ' Funcion para saltarse bloqueos tipo contrase�as cuando estas en el VB'''
        If InStr(My.Application.Info.DirectoryPath, "work", CompareMethod.Text) > 0 Then
            Dise�o = True
        Else
            Dise�o = False
        End If
    End Function

    Public Function LeerIni(ByVal lpFileName As String, ByVal lpAppName As String, ByVal lpKeyName As String, Optional ByVal vDefault As String = "") As String

        Dim LTmp As Long
        Dim sRetVal As String

        Dim fso As New System.IO.FileInfo(lpFileName)
        'me fijo si existe el archivo *.ini
        If fso.Exists = True Then

            sRetVal = New String(Chr(0), 255)

            LTmp = GetPrivateProfileString(lpAppName, lpKeyName, vDefault, sRetVal, Len(sRetVal), lpFileName)
            If LTmp = 0 Then
                LeerIni = vDefault
            Else
                LeerIni = Left(sRetVal, CInt(LTmp))
            End If
        Else
            LeerIni = "-1"
        End If
        fso = Nothing
    End Function

End Module
