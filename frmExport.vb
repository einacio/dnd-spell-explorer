Option Strict Off
Public Class frmExport
    Private idsConjuros As New List(Of String)
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExport.Click
        If idsConjuros.Count > 0 Then
            Dim ap As Microsoft.Office.Interop.Word.Application
            Dim a As Microsoft.Office.Interop.Word.Document
            '        Dim count As Object
            'ap = New Microsoft.Office.Interop.Word.Application
            Try 'getobject me devuelve una instancia existente de word, sino creo una yo
                ap = CType(GetObject(, "Word.Application"), Microsoft.Office.Interop.Word.Application)
            Catch ex As System.Exception
                ap = New Microsoft.Office.Interop.Word.Application()
            End Try
            a = ap.Documents.Add()

            a.ShowSpellingErrors = False
            a.ShowGrammaticalErrors = False
            a.PageSetup.LeftMargin = 23
            a.PageSetup.RightMargin = 23
            a.PageSetup.TextColumns.SetCount(2)

            'ap.Visible = True
            'a = ap.Documents(1)
            Using cn As New System.Data.OleDb.OleDbConnection(connStr)
                cn.Open()
                Dim cm As OleDb.OleDbCommand
                Dim rs As OleDb.OleDbDataReader
                Dim idConjuro As Integer
                Dim nivel As Integer
                Dim nivelAnt As Integer
                Dim temp As String
                Dim clase As String
                Dim lista As String = Join(idsConjuros.ToArray, ",")
                clase = ""
                cm = New OleDb.OleDbCommand()
                ''''Resumen de Conjuros
                If chkMin.Checked Then
                    With cm
                        .Connection = cn
                        .CommandType = CommandType.Text
                        .CommandText = "select v_conjuros.nombre_ing, v_conjuros.descripcioncorta_ing ,v_conjuros_clases.nivel, v_conjuros_clases.clase_ing, v_conjuros_clases.tipo from v_conjuros inner join v_conjuros_clases on v_conjuros_clases.idconjuro=v_conjuros.idconjuro where v_conjuros.idconjuro in(" & lista & ") order by clase_ing, nivel, nombre_ing"
                        rs = .ExecuteReader(CommandBehavior.SingleResult)
                    End With
                    While rs.Read()
                        nivel = CInt(rs.Item("nivel"))
                        With a
                            If clase <> rs("clase_ing").ToString().ToUpper() Then
                                clase = rs("clase_ing").ToString().ToUpper()
                                nivelAnt = -1
                            End If
                            If nivel > nivelAnt Then
                                temp = clase & " SPELLS"
                                If nivelAnt = -1 Then
                                    .Range(.Range.End - 1).Font.Bold = Microsoft.Office.Interop.Word.WdConstants.wdToggle
                                    .Range(.Range.End - 1).Font.Size = 13
                                    .Range.InsertAfter(temp & vbCrLf)
                                End If
                                nivelAnt = nivel
                                Select Case nivel
                                    Case 0
                                        temp = "0-LEVEL " & temp
                                        If rs("tipo").ToString = "D" Then
                                            temp &= " (ORISONS)"
                                        ElseIf rs("tipo").ToString = "A" Then
                                            temp &= " (CANTRIPS)"
                                        End If
                                    Case 1
                                        temp = "1ST-LEVEL " & temp
                                    Case 2
                                        temp = "2ND-LEVEL " & temp
                                    Case 3
                                        temp = "3RD-LEVEL " & temp
                                    Case Else
                                        temp = nivel.ToString() & "TH-LEVEL " & temp
                                End Select
                                .Range(.Range.End - 1).Font.Bold = CInt(False)
                                .Range(.Range.End - 1).Font.Size = 11
                                .Range.InsertAfter(temp & vbCrLf)
                            End If
                            'pongo negritas, escribo el nombre, la descripcion,  y despues le saco negritas a la descripcion
                            'el problema estaba en que mantenia negritas para todo el parrafo si no la defino por bloques
                            .Range(.Range.End - 1).Font.Size = 10
                            temp = rs("nombre_ing").ToString() & ": "
                            .Range.InsertAfter(temp)
                            .Range(.Range.End - 2 - temp.Length, .Range.End - 2).Font.Bold = 1
                            .Range.InsertAfter(rs("descripcioncorta_ing").ToString() & vbCrLf)
                        End With
                    End While
                    rs.Close()
                    ''''fin resumen
                    'agrego seccion nueva para aplicarle estilo 2 columnas
                    a.Sections.Add()
                    a.Range.InsertAfter(vbCrLf & vbCrLf)
                    a.Sections(2).PageSetup.TextColumns.SetCount(1)
                    a.Sections(2).PageSetup.SectionStart = Microsoft.Office.Interop.Word.WdSectionStart.wdSectionContinuous
                    a.Sections.Add()
                End If
                a.Sections.Last.PageSetup.TextColumns.SetCount(3)
                a.Sections.Last.PageSetup.SectionStart = Microsoft.Office.Interop.Word.WdSectionStart.wdSectionContinuous
                a.Range(a.Range.End - 1).Font.Size = 10
                Dim rs2 As OleDb.OleDbDataReader
                'Conjuros detallado
                'a.Range(a.Range.End - 1).Text = vbCrLf & vbCrLf & "SPELL DESCRIPTIONS"
                a.Range.InsertAfter("SPELL DESCRIPTIONS")
                a.Range(a.Range.End - 19, a.Range.End - 1).Font.Bold = 1 'Microsoft.Office.Interop.Word.WdConstants.wdToggle
                a.Range(a.Range.End - 19, a.Range.End - 1).Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle
                a.Range(a.Range.End - 19, a.Range.End - 1).Font.Size = 13
                'a.Range(a.Range.End - 19, a.Range.End - 2).ParagraphFormat.SpaceAfter = 3
                a.Range.InsertAfter(vbCrLf)
                'a.Range(a.Range.End - 2, a.Range.End - 1).ParagraphFormat.SpaceAfter = 0
                With cm
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select v_conjuros.* from v_conjuros where idconjuro in (" & lista & ") order by nombre_ing"
                    rs = .ExecuteReader(CommandBehavior.SingleResult)
                End With
                While rs.Read()
                    With a
                        idConjuro = CInt(rs.Item("idconjuro"))
                        temp = rs.Item("nombre_ing").ToString
                        .Range.InsertAfter(temp)
                        .Range.InsertAfter(vbCrLf)
                        .Range(.Range.End - 2 - temp.Length, .Range.End - 2).Font.Bold = 1
                        .Range(.Range.End - 1).Font.Bold = 0
                        .Range.InsertAfter("Manual: ")
                        .Range(.Range.End - 9, .Range.End - 2).Font.Bold = 1
                        .Range.InsertAfter(rs("manual").ToString & vbCrLf)
                        .Range.InsertAfter(rs.Item("escuela_ing").ToString)
                        If rs.Item("subescuela_ing").ToString <> "" Then
                            .Range.InsertAfter("(" & rs.Item("subescuela_ing").ToString & ")")
                        End If
                        cm = New OleDb.OleDbCommand()
                        With cm
                            .Connection = cn
                            .CommandType = CommandType.Text
                            .CommandText = "select * from v_conjuros_descriptores where idconjuro=@idconjuro order by descriptor_ing"
                            .Parameters.AddWithValue("@idconjuro", idConjuro)
                            rs2 = .ExecuteReader(CommandBehavior.SingleResult)
                        End With
                        If rs2.HasRows Then
                            temp = " ["
                            While rs2.Read()
                                temp &= rs2("descriptor_ing").ToString() & ", "
                            End While
                            temp = temp.Remove(temp.Length - 2) & "]"
                            .Range.InsertAfter(temp)
                        End If
                        rs2.Close()
                        .Range.InsertAfter(vbCrLf & "Level: ")
                        .Range(.Range.End - 9, .Range.End - 2).Font.Bold = 1
                        temp = ""
                        cm = New OleDb.OleDbCommand()
                        With cm
                            .Connection = cn
                            .CommandType = CommandType.Text
                            .CommandText = "select * from v_conjuros_clases where idconjuro=@idconjuro"
                            .Parameters.AddWithValue("@idconjuro", idConjuro)
                            rs2 = .ExecuteReader(CommandBehavior.SingleResult)
                        End With
                        While rs2.Read()
                            temp &= (rs2("clase_ing").ToString() & " " & rs2("nivel").ToString()) & ", "
                        End While
                        rs2.Close()
                        .Range.InsertAfter(temp.Remove(temp.Length - 2) & vbCrLf)
                        temp = ""
                        .Range.InsertAfter("Components: ")
                        .Range(.Range.End - 13, .Range.End - 2).Font.Bold = 1
                        If CBool(rs.Item("V")) = True Then
                            temp &= "V, "
                        End If
                        If CBool(rs.Item("S")) = True Then
                            temp &= "S, "
                        End If
                        If CBool(rs.Item("M")) = True Then
                            If CBool(rs.Item("M/DF")) = True Then
                                temp &= "M/DF, "
                            Else
                                temp &= "M, "
                            End If
                        End If
                        If CBool(rs.Item("F")) = True Then
                            If CBool(rs.Item("F/DF")) = True Then
                                temp &= "F/DF, "
                            Else
                                temp &= "F, "
                            End If
                        End If
                        If CBool(rs.Item("DF")) = True Then
                            If temp.IndexOf("DF") <= 0 Then
                                temp &= "DF, "
                            End If
                        End If
                        If CBool(rs.Item("XP")) = True Then
                            temp &= "XP, "
                        End If
                        .Range.InsertAfter(temp.Remove(temp.Length - 2) & vbCrLf)

                        .Range.InsertAfter("Casting time: ")
                        .Range(.Range.End - 15, .Range.End - 2).Font.Bold = 1
                        .Range.InsertAfter(rs("tiempo_ing").ToString & vbCrLf)

                        .Range.InsertAfter("Range: ")
                        .Range(.Range.End - 8, .Range.End - 2).Font.Bold = 1
                        .Range.InsertAfter(rs("alcance_ing").ToString & vbCrLf)

                        If rs("objetivo_ing").ToString <> "" Then
                            .Range.InsertAfter("Target: ")
                            .Range(.Range.End - 9, .Range.End - 2).Font.Bold = 1
                            .Range.InsertAfter(rs("objetivo_ing").ToString & vbCrLf)
                        End If

                        If rs("efecto_ing").ToString <> "" Then
                            .Range.InsertAfter("Effect: ")
                            .Range(.Range.End - 9, .Range.End - 2).Font.Bold = 1
                            .Range.InsertAfter(rs("efecto_ing").ToString & vbCrLf)
                        End If
                        If rs("area_ing").ToString <> "" Then
                            .Range.InsertAfter("Area: ")
                            .Range(.Range.End - 7, .Range.End - 2).Font.Bold = 1
                            .Range.InsertAfter(rs("area_ing").ToString & vbCrLf)
                        End If

                        .Range.InsertAfter("Duration: ")
                        .Range(.Range.End - 11, .Range.End - 2).Font.Bold = 1
                        .Range.InsertAfter(rs("duracion_ing").ToString & vbCrLf)

                        .Range.InsertAfter("Saving throw: ")
                        .Range(.Range.End - 15, .Range.End - 2).Font.Bold = 1
                        .Range.InsertAfter(rs("salvacion_ing").ToString & vbCrLf)

                        If rs("resistencia_ing").ToString <> "" Then
                            .Range.InsertAfter("Spell resistance: ")
                            .Range(.Range.End - 19, .Range.End - 2).Font.Bold = 1
                            .Range.InsertAfter(rs("resistencia_ing").ToString & vbCrLf)
                        End If

                        .Range.InsertAfter(rs("descripcion_ing").ToString & vbCrLf & vbCrLf)

                    End With
                End While
                ''fin detalle

                cn.Close()
            End Using

            'a.Range(0).Text = "blah"
            'count = a.Range.End - 1
            'a.Range(count).Text = vbCrLf
            'count = a.Range.End - 1
            'a.Range(count).Bold = 1
            'a.Range(count).Text = "bleh"
            'count = a.Range.End - 1
            'a.Range(count).Bold = 0
            'a.SaveAs("c:\blah.doc")
            'a.Close()
            'ap.Quit()
            ap.Visible = True
            ap.Activate()

            a = Nothing
            ap = Nothing
        End If
    End Sub

    Private Sub frmExport_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Label1.BackColor = SystemColors.ActiveCaption
        Label1.ForeColor = SystemColors.ActiveCaptionText
    End Sub

    Private Sub frmExport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Height = frmSpell.Height
        Me.Owner = frmSpell
    End Sub

    Private Sub frmExport_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        Label1.BackColor = SystemColors.InactiveCaption
        Label1.ForeColor = SystemColors.InactiveCaptionText
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAgregar.Click
        Dim idConjuro As String
        Dim nombre As String
        idConjuro = frmSpell.idConjuroActual.ToString
        If idConjuro <> "0" Then
            If Not idsConjuros.Contains(idConjuro) Then
                Using cn As New OleDb.OleDbConnection(connStr)
                    Dim cm As OleDb.OleDbCommand
                    cn.Open()
                    cm = New OleDb.OleDbCommand
                    With cm
                        .Connection = cn
                        .CommandType = CommandType.Text
                        .CommandText = "select nombre_ing from conjuros where idconjuro=@id"
                        .Parameters.AddWithValue("@id", CInt(idConjuro))
                        nombre = .ExecuteScalar().ToString
                    End With
                    cn.Close()
                End Using
                ListBox1.Items.Add(nombre)
                idsConjuros.Add(idConjuro)
            End If
        End If
    End Sub

    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        If ListBox1.SelectedIndex > -1 Then
            idsConjuros.RemoveAt(ListBox1.SelectedIndex)
            ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
        End If
    End Sub

    Private Sub cmdLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLimpiar.Click
        ListBox1.Items.Clear()
        idsConjuros.Clear()
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        'Dim sPath As String = System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).ToString
        '' Open the XML file.
        'Dim xmlDoc As New System.Xml.XmlDocument()
        'xmlDoc.Load(sPath & "..\..\xmlfile1.xml")
        '' Open the XSL file.
        'Dim xslDoc As New System.Xml.Xsl.XslCompiledTransform()
        'xslDoc.Load(sPath & "..\..\xsltfile1.xslt")
        '' Transform the XSL and save it to a file.
        'Dim TWrtr As New System.Xml.XmlTextWriter(sPath & "\Dictionary.RTF", System.Text.Encoding.Default)
        'xslDoc.Transform(xmlDoc, Nothing, TWrtr)
        'TWrtr.Close()
        'MsgBox("Transformed RTF saved to " & sPath & "\Dictionary.RTF")

    End Sub
End Class