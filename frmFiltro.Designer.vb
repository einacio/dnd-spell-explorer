<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFiltro
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Label1 = New System.Windows.Forms.Label
        Me.TVRes = New System.Windows.Forms.TreeView
        Me.mnuTreeView = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExpandirTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ContraerTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.cmbOrden = New System.Windows.Forms.ComboBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.grpDescriptor = New System.Windows.Forms.GroupBox
        Me.lstDescriptor = New System.Windows.Forms.CheckedListBox
        Me.lnkDescriptor = New System.Windows.Forms.LinkLabel
        Me.grpEscuela = New System.Windows.Forms.GroupBox
        Me.lstEscuela = New System.Windows.Forms.CheckedListBox
        Me.lnkEscuela = New System.Windows.Forms.LinkLabel
        Me.grpMan = New System.Windows.Forms.GroupBox
        Me.lstManuales = New System.Windows.Forms.CheckedListBox
        Me.lnkMan = New System.Windows.Forms.LinkLabel
        Me.grpTexto = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtTextoOR = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtTextoAND = New System.Windows.Forms.TextBox
        Me.lnkTexto = New System.Windows.Forms.LinkLabel
        Me.grpMat = New System.Windows.Forms.GroupBox
        Me.chkXP = New System.Windows.Forms.CheckBox
        Me.chkDF = New System.Windows.Forms.CheckBox
        Me.chkF = New System.Windows.Forms.CheckBox
        Me.chkXM = New System.Windows.Forms.CheckBox
        Me.chkM = New System.Windows.Forms.CheckBox
        Me.chkS = New System.Windows.Forms.CheckBox
        Me.chkV = New System.Windows.Forms.CheckBox
        Me.lnkMat = New System.Windows.Forms.LinkLabel
        Me.grpNivel = New System.Windows.Forms.GroupBox
        Me.lstNivel = New System.Windows.Forms.CheckedListBox
        Me.lnkNivel = New System.Windows.Forms.LinkLabel
        Me.grpClases = New System.Windows.Forms.GroupBox
        Me.lstClases = New System.Windows.Forms.CheckedListBox
        Me.lnkClases = New System.Windows.Forms.LinkLabel
        Me.cmdFiltrar = New System.Windows.Forms.Button
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel
        Me.mnuTreeView.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.grpDescriptor.SuspendLayout()
        Me.grpEscuela.SuspendLayout()
        Me.grpMan.SuspendLayout()
        Me.grpTexto.SuspendLayout()
        Me.grpMat.SuspendLayout()
        Me.grpNivel.SuspendLayout()
        Me.grpClases.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(0, -1)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(191, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Busqueda"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TVRes
        '
        Me.TVRes.ContextMenuStrip = Me.mnuTreeView
        Me.TVRes.Location = New System.Drawing.Point(3, 46)
        Me.TVRes.Margin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.TVRes.Name = "TVRes"
        Me.TVRes.Size = New System.Drawing.Size(185, 387)
        Me.TVRes.TabIndex = 3
        '
        'mnuTreeView
        '
        Me.mnuTreeView.BackColor = System.Drawing.SystemColors.MenuBar
        Me.mnuTreeView.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExpandirTodoToolStripMenuItem, Me.ContraerTodoToolStripMenuItem})
        Me.mnuTreeView.Name = "ContextMenuStrip1"
        Me.mnuTreeView.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuTreeView.ShowImageMargin = False
        Me.mnuTreeView.Size = New System.Drawing.Size(131, 48)
        '
        'ExpandirTodoToolStripMenuItem
        '
        Me.ExpandirTodoToolStripMenuItem.Name = "ExpandirTodoToolStripMenuItem"
        Me.ExpandirTodoToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.ExpandirTodoToolStripMenuItem.Text = "Expandir Todo"
        '
        'ContraerTodoToolStripMenuItem
        '
        Me.ContraerTodoToolStripMenuItem.Name = "ContraerTodoToolStripMenuItem"
        Me.ContraerTodoToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.ContraerTodoToolStripMenuItem.Text = "Contraer Todo"
        '
        'cmbOrden
        '
        Me.cmbOrden.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOrden.FormattingEnabled = True
        Me.cmbOrden.Items.AddRange(New Object() {"Por Clase y Nivel", "Por Letra", "Por Manual, Clase y Nivel", "Libre"})
        Me.cmbOrden.Location = New System.Drawing.Point(12, 436)
        Me.cmbOrden.Name = "cmbOrden"
        Me.cmbOrden.Size = New System.Drawing.Size(166, 21)
        Me.cmbOrden.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grpDescriptor)
        Me.GroupBox1.Controls.Add(Me.grpEscuela)
        Me.GroupBox1.Controls.Add(Me.grpMan)
        Me.GroupBox1.Controls.Add(Me.grpTexto)
        Me.GroupBox1.Controls.Add(Me.grpMat)
        Me.GroupBox1.Controls.Add(Me.grpNivel)
        Me.GroupBox1.Controls.Add(Me.grpClases)
        Me.GroupBox1.Controls.Add(Me.cmdFiltrar)
        Me.GroupBox1.Controls.Add(Me.LinkLabel1)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 23)
        Me.GroupBox1.MinimumSize = New System.Drawing.Size(185, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.GroupBox1.Size = New System.Drawing.Size(185, 20)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'grpDescriptor
        '
        Me.grpDescriptor.Controls.Add(Me.lstDescriptor)
        Me.grpDescriptor.Controls.Add(Me.lnkDescriptor)
        Me.grpDescriptor.Location = New System.Drawing.Point(9, 126)
        Me.grpDescriptor.MaximumSize = New System.Drawing.Size(170, 200)
        Me.grpDescriptor.MinimumSize = New System.Drawing.Size(170, 20)
        Me.grpDescriptor.Name = "grpDescriptor"
        Me.grpDescriptor.Size = New System.Drawing.Size(170, 20)
        Me.grpDescriptor.TabIndex = 8
        Me.grpDescriptor.TabStop = False
        '
        'lstDescriptor
        '
        Me.lstDescriptor.CheckOnClick = True
        Me.lstDescriptor.FormattingEnabled = True
        Me.lstDescriptor.Location = New System.Drawing.Point(6, 20)
        Me.lstDescriptor.Name = "lstDescriptor"
        Me.lstDescriptor.Size = New System.Drawing.Size(158, 169)
        Me.lstDescriptor.TabIndex = 1
        '
        'lnkDescriptor
        '
        Me.lnkDescriptor.AutoSize = True
        Me.lnkDescriptor.Location = New System.Drawing.Point(7, -2)
        Me.lnkDescriptor.Name = "lnkDescriptor"
        Me.lnkDescriptor.Size = New System.Drawing.Size(66, 13)
        Me.lnkDescriptor.TabIndex = 0
        Me.lnkDescriptor.TabStop = True
        Me.lnkDescriptor.Text = "Descriptores"
        Me.lnkDescriptor.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'grpEscuela
        '
        Me.grpEscuela.Controls.Add(Me.lstEscuela)
        Me.grpEscuela.Controls.Add(Me.lnkEscuela)
        Me.grpEscuela.Location = New System.Drawing.Point(9, 100)
        Me.grpEscuela.MaximumSize = New System.Drawing.Size(170, 200)
        Me.grpEscuela.MinimumSize = New System.Drawing.Size(170, 20)
        Me.grpEscuela.Name = "grpEscuela"
        Me.grpEscuela.Size = New System.Drawing.Size(170, 20)
        Me.grpEscuela.TabIndex = 7
        Me.grpEscuela.TabStop = False
        '
        'lstEscuela
        '
        Me.lstEscuela.CheckOnClick = True
        Me.lstEscuela.FormattingEnabled = True
        Me.lstEscuela.Location = New System.Drawing.Point(6, 20)
        Me.lstEscuela.Name = "lstEscuela"
        Me.lstEscuela.Size = New System.Drawing.Size(158, 169)
        Me.lstEscuela.TabIndex = 1
        '
        'lnkEscuela
        '
        Me.lnkEscuela.AutoSize = True
        Me.lnkEscuela.Location = New System.Drawing.Point(7, -2)
        Me.lnkEscuela.Name = "lnkEscuela"
        Me.lnkEscuela.Size = New System.Drawing.Size(45, 13)
        Me.lnkEscuela.TabIndex = 0
        Me.lnkEscuela.TabStop = True
        Me.lnkEscuela.Text = "Escuela"
        Me.lnkEscuela.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'grpMan
        '
        Me.grpMan.Controls.Add(Me.lstManuales)
        Me.grpMan.Controls.Add(Me.lnkMan)
        Me.grpMan.Location = New System.Drawing.Point(9, 152)
        Me.grpMan.MaximumSize = New System.Drawing.Size(170, 200)
        Me.grpMan.MinimumSize = New System.Drawing.Size(170, 20)
        Me.grpMan.Name = "grpMan"
        Me.grpMan.Size = New System.Drawing.Size(170, 20)
        Me.grpMan.TabIndex = 3
        Me.grpMan.TabStop = False
        '
        'lstManuales
        '
        Me.lstManuales.CheckOnClick = True
        Me.lstManuales.FormattingEnabled = True
        Me.lstManuales.Location = New System.Drawing.Point(6, 20)
        Me.lstManuales.Name = "lstManuales"
        Me.lstManuales.Size = New System.Drawing.Size(158, 169)
        Me.lstManuales.TabIndex = 1
        '
        'lnkMan
        '
        Me.lnkMan.AutoSize = True
        Me.lnkMan.Location = New System.Drawing.Point(7, -2)
        Me.lnkMan.Name = "lnkMan"
        Me.lnkMan.Size = New System.Drawing.Size(42, 13)
        Me.lnkMan.TabIndex = 0
        Me.lnkMan.TabStop = True
        Me.lnkMan.Text = "Manual"
        Me.lnkMan.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'grpTexto
        '
        Me.grpTexto.Controls.Add(Me.Label3)
        Me.grpTexto.Controls.Add(Me.txtTextoOR)
        Me.grpTexto.Controls.Add(Me.Label2)
        Me.grpTexto.Controls.Add(Me.txtTextoAND)
        Me.grpTexto.Controls.Add(Me.lnkTexto)
        Me.grpTexto.Location = New System.Drawing.Point(9, 178)
        Me.grpTexto.MaximumSize = New System.Drawing.Size(170, 110)
        Me.grpTexto.MinimumSize = New System.Drawing.Size(170, 20)
        Me.grpTexto.Name = "grpTexto"
        Me.grpTexto.Size = New System.Drawing.Size(170, 20)
        Me.grpTexto.TabIndex = 5
        Me.grpTexto.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Alguna de estas palabras"
        '
        'txtTextoOR
        '
        Me.txtTextoOR.AllowDrop = True
        Me.txtTextoOR.Location = New System.Drawing.Point(6, 83)
        Me.txtTextoOR.Name = "txtTextoOR"
        Me.txtTextoOR.Size = New System.Drawing.Size(158, 20)
        Me.txtTextoOR.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Todas estas palabras"
        '
        'txtTextoAND
        '
        Me.txtTextoAND.AllowDrop = True
        Me.txtTextoAND.Location = New System.Drawing.Point(6, 37)
        Me.txtTextoAND.Name = "txtTextoAND"
        Me.txtTextoAND.Size = New System.Drawing.Size(158, 20)
        Me.txtTextoAND.TabIndex = 1
        '
        'lnkTexto
        '
        Me.lnkTexto.AutoSize = True
        Me.lnkTexto.Location = New System.Drawing.Point(7, -2)
        Me.lnkTexto.Name = "lnkTexto"
        Me.lnkTexto.Size = New System.Drawing.Size(34, 13)
        Me.lnkTexto.TabIndex = 0
        Me.lnkTexto.TabStop = True
        Me.lnkTexto.Text = "Texto"
        Me.lnkTexto.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'grpMat
        '
        Me.grpMat.Controls.Add(Me.chkXP)
        Me.grpMat.Controls.Add(Me.chkDF)
        Me.grpMat.Controls.Add(Me.chkF)
        Me.grpMat.Controls.Add(Me.chkXM)
        Me.grpMat.Controls.Add(Me.chkM)
        Me.grpMat.Controls.Add(Me.chkS)
        Me.grpMat.Controls.Add(Me.chkV)
        Me.grpMat.Controls.Add(Me.lnkMat)
        Me.grpMat.Location = New System.Drawing.Point(9, 74)
        Me.grpMat.MaximumSize = New System.Drawing.Size(170, 160)
        Me.grpMat.MinimumSize = New System.Drawing.Size(170, 20)
        Me.grpMat.Name = "grpMat"
        Me.grpMat.Size = New System.Drawing.Size(170, 20)
        Me.grpMat.TabIndex = 2
        Me.grpMat.TabStop = False
        '
        'chkXP
        '
        Me.chkXP.AutoSize = True
        Me.chkXP.Location = New System.Drawing.Point(27, 135)
        Me.chkXP.Name = "chkXP"
        Me.chkXP.Size = New System.Drawing.Size(81, 17)
        Me.chkXP.TabIndex = 10
        Me.chkXP.TabStop = False
        Me.chkXP.Text = "Experiencia"
        Me.chkXP.UseMnemonic = False
        Me.chkXP.UseVisualStyleBackColor = True
        '
        'chkDF
        '
        Me.chkDF.AutoSize = True
        Me.chkDF.Location = New System.Drawing.Point(27, 112)
        Me.chkDF.Name = "chkDF"
        Me.chkDF.Size = New System.Drawing.Size(83, 17)
        Me.chkDF.TabIndex = 11
        Me.chkDF.TabStop = False
        Me.chkDF.Text = "Foco Divino"
        Me.chkDF.UseMnemonic = False
        Me.chkDF.UseVisualStyleBackColor = True
        '
        'chkF
        '
        Me.chkF.AutoSize = True
        Me.chkF.Location = New System.Drawing.Point(27, 89)
        Me.chkF.Name = "chkF"
        Me.chkF.Size = New System.Drawing.Size(50, 17)
        Me.chkF.TabIndex = 12
        Me.chkF.TabStop = False
        Me.chkF.Text = "Foco"
        Me.chkF.UseMnemonic = False
        Me.chkF.UseVisualStyleBackColor = True
        '
        'chkXM
        '
        Me.chkXM.AutoSize = True
        Me.chkXM.Location = New System.Drawing.Point(96, 66)
        Me.chkXM.Name = "chkXM"
        Me.chkXM.Size = New System.Drawing.Size(48, 17)
        Me.chkXM.TabIndex = 9
        Me.chkXM.TabStop = False
        Me.chkXM.Text = "Caro"
        Me.chkXM.UseMnemonic = False
        Me.chkXM.UseVisualStyleBackColor = True
        '
        'chkM
        '
        Me.chkM.AutoSize = True
        Me.chkM.Location = New System.Drawing.Point(27, 66)
        Me.chkM.Name = "chkM"
        Me.chkM.Size = New System.Drawing.Size(63, 17)
        Me.chkM.TabIndex = 6
        Me.chkM.TabStop = False
        Me.chkM.Text = "Material"
        Me.chkM.UseMnemonic = False
        Me.chkM.UseVisualStyleBackColor = True
        '
        'chkS
        '
        Me.chkS.AutoSize = True
        Me.chkS.Location = New System.Drawing.Point(27, 43)
        Me.chkS.Name = "chkS"
        Me.chkS.Size = New System.Drawing.Size(70, 17)
        Me.chkS.TabIndex = 7
        Me.chkS.TabStop = False
        Me.chkS.Text = "Somatico"
        Me.chkS.UseMnemonic = False
        Me.chkS.UseVisualStyleBackColor = True
        '
        'chkV
        '
        Me.chkV.AutoSize = True
        Me.chkV.Location = New System.Drawing.Point(27, 20)
        Me.chkV.Name = "chkV"
        Me.chkV.Size = New System.Drawing.Size(56, 17)
        Me.chkV.TabIndex = 8
        Me.chkV.TabStop = False
        Me.chkV.Text = "Verbal"
        Me.chkV.UseMnemonic = False
        Me.chkV.UseVisualStyleBackColor = True
        '
        'lnkMat
        '
        Me.lnkMat.AutoSize = True
        Me.lnkMat.Location = New System.Drawing.Point(7, -2)
        Me.lnkMat.Name = "lnkMat"
        Me.lnkMat.Size = New System.Drawing.Size(55, 13)
        Me.lnkMat.TabIndex = 0
        Me.lnkMat.TabStop = True
        Me.lnkMat.Text = "Materiales"
        Me.lnkMat.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'grpNivel
        '
        Me.grpNivel.Controls.Add(Me.lstNivel)
        Me.grpNivel.Controls.Add(Me.lnkNivel)
        Me.grpNivel.Location = New System.Drawing.Point(9, 48)
        Me.grpNivel.MaximumSize = New System.Drawing.Size(170, 180)
        Me.grpNivel.MinimumSize = New System.Drawing.Size(170, 20)
        Me.grpNivel.Name = "grpNivel"
        Me.grpNivel.Size = New System.Drawing.Size(170, 20)
        Me.grpNivel.TabIndex = 4
        Me.grpNivel.TabStop = False
        '
        'lstNivel
        '
        Me.lstNivel.CheckOnClick = True
        Me.lstNivel.FormattingEnabled = True
        Me.lstNivel.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"})
        Me.lstNivel.Location = New System.Drawing.Point(6, 20)
        Me.lstNivel.Name = "lstNivel"
        Me.lstNivel.Size = New System.Drawing.Size(158, 154)
        Me.lstNivel.TabIndex = 1
        '
        'lnkNivel
        '
        Me.lnkNivel.AutoSize = True
        Me.lnkNivel.Location = New System.Drawing.Point(7, -2)
        Me.lnkNivel.Name = "lnkNivel"
        Me.lnkNivel.Size = New System.Drawing.Size(31, 13)
        Me.lnkNivel.TabIndex = 0
        Me.lnkNivel.TabStop = True
        Me.lnkNivel.Text = "Nivel"
        Me.lnkNivel.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'grpClases
        '
        Me.grpClases.Controls.Add(Me.lstClases)
        Me.grpClases.Controls.Add(Me.lnkClases)
        Me.grpClases.Location = New System.Drawing.Point(9, 22)
        Me.grpClases.MaximumSize = New System.Drawing.Size(170, 200)
        Me.grpClases.MinimumSize = New System.Drawing.Size(170, 20)
        Me.grpClases.Name = "grpClases"
        Me.grpClases.Size = New System.Drawing.Size(170, 20)
        Me.grpClases.TabIndex = 1
        Me.grpClases.TabStop = False
        '
        'lstClases
        '
        Me.lstClases.CheckOnClick = True
        Me.lstClases.FormattingEnabled = True
        Me.lstClases.Location = New System.Drawing.Point(6, 20)
        Me.lstClases.Name = "lstClases"
        Me.lstClases.Size = New System.Drawing.Size(158, 169)
        Me.lstClases.TabIndex = 1
        Me.lstClases.ThreeDCheckBoxes = True
        '
        'lnkClases
        '
        Me.lnkClases.AutoSize = True
        Me.lnkClases.Location = New System.Drawing.Point(7, -2)
        Me.lnkClases.Name = "lnkClases"
        Me.lnkClases.Size = New System.Drawing.Size(38, 13)
        Me.lnkClases.TabIndex = 0
        Me.lnkClases.TabStop = True
        Me.lnkClases.Text = "Clases"
        Me.lnkClases.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'cmdFiltrar
        '
        Me.cmdFiltrar.Location = New System.Drawing.Point(19, 204)
        Me.cmdFiltrar.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.cmdFiltrar.Name = "cmdFiltrar"
        Me.cmdFiltrar.Size = New System.Drawing.Size(134, 27)
        Me.cmdFiltrar.TabIndex = 6
        Me.cmdFiltrar.Text = "Filtrar"
        Me.cmdFiltrar.UseVisualStyleBackColor = True
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(9, -1)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(32, 13)
        Me.LinkLabel1.TabIndex = 0
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Filtrar"
        Me.LinkLabel1.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'frmFiltro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(190, 461)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmbOrden)
        Me.Controls.Add(Me.TVRes)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmFiltro"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.mnuTreeView.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpDescriptor.ResumeLayout(False)
        Me.grpDescriptor.PerformLayout()
        Me.grpEscuela.ResumeLayout(False)
        Me.grpEscuela.PerformLayout()
        Me.grpMan.ResumeLayout(False)
        Me.grpMan.PerformLayout()
        Me.grpTexto.ResumeLayout(False)
        Me.grpTexto.PerformLayout()
        Me.grpMat.ResumeLayout(False)
        Me.grpMat.PerformLayout()
        Me.grpNivel.ResumeLayout(False)
        Me.grpNivel.PerformLayout()
        Me.grpClases.ResumeLayout(False)
        Me.grpClases.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TVRes As System.Windows.Forms.TreeView
    Friend WithEvents cmbOrden As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents grpMat As System.Windows.Forms.GroupBox
    Friend WithEvents lnkMat As System.Windows.Forms.LinkLabel
    Friend WithEvents grpClases As System.Windows.Forms.GroupBox
    Friend WithEvents lnkClases As System.Windows.Forms.LinkLabel
    Friend WithEvents cmdFiltrar As System.Windows.Forms.Button
    Friend WithEvents grpTexto As System.Windows.Forms.GroupBox
    Friend WithEvents lnkTexto As System.Windows.Forms.LinkLabel
    Friend WithEvents grpNivel As System.Windows.Forms.GroupBox
    Friend WithEvents lnkNivel As System.Windows.Forms.LinkLabel
    Friend WithEvents grpMan As System.Windows.Forms.GroupBox
    Friend WithEvents lnkMan As System.Windows.Forms.LinkLabel
    Friend WithEvents lstClases As System.Windows.Forms.CheckedListBox
    Friend WithEvents lstNivel As System.Windows.Forms.CheckedListBox
    Friend WithEvents chkXP As System.Windows.Forms.CheckBox
    Friend WithEvents chkDF As System.Windows.Forms.CheckBox
    Friend WithEvents chkF As System.Windows.Forms.CheckBox
    Friend WithEvents chkXM As System.Windows.Forms.CheckBox
    Friend WithEvents chkM As System.Windows.Forms.CheckBox
    Friend WithEvents chkS As System.Windows.Forms.CheckBox
    Friend WithEvents chkV As System.Windows.Forms.CheckBox
    Friend WithEvents txtTextoAND As System.Windows.Forms.TextBox
    Friend WithEvents lstManuales As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTextoOR As System.Windows.Forms.TextBox
    Friend WithEvents mnuTreeView As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ExpandirTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContraerTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents grpEscuela As System.Windows.Forms.GroupBox
    Friend WithEvents lstEscuela As System.Windows.Forms.CheckedListBox
    Friend WithEvents lnkEscuela As System.Windows.Forms.LinkLabel
    Friend WithEvents grpDescriptor As System.Windows.Forms.GroupBox
    Friend WithEvents lstDescriptor As System.Windows.Forms.CheckedListBox
    Friend WithEvents lnkDescriptor As System.Windows.Forms.LinkLabel
End Class
