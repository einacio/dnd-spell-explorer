<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSpell
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSpell))
        Me.txtIdConjuro = New System.Windows.Forms.TextBox
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.txtDescCorta = New System.Windows.Forms.TextBox
        Me.txtDescLarga = New System.Windows.Forms.TextBox
        Me.ListClases = New System.Windows.Forms.ListBox
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.ListDatos = New System.Windows.Forms.ListBox
        Me.cmdGuardar = New System.Windows.Forms.Button
        Me.cmdNew = New System.Windows.Forms.Button
        Me.cmdExport = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.cmdFiltro = New System.Windows.Forms.Button
        Me.txtManual = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkXP = New System.Windows.Forms.CheckBox
        Me.chkDF = New System.Windows.Forms.CheckBox
        Me.chkF = New System.Windows.Forms.CheckBox
        Me.chkXM = New System.Windows.Forms.CheckBox
        Me.chkM = New System.Windows.Forms.CheckBox
        Me.chkS = New System.Windows.Forms.CheckBox
        Me.chkV = New System.Windows.Forms.CheckBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtIdConjuro
        '
        Me.txtIdConjuro.Location = New System.Drawing.Point(12, 14)
        Me.txtIdConjuro.Name = "txtIdConjuro"
        Me.txtIdConjuro.Size = New System.Drawing.Size(48, 20)
        Me.txtIdConjuro.TabIndex = 0
        Me.txtIdConjuro.Visible = False
        '
        'txtNombre
        '
        Me.txtNombre.BackColor = System.Drawing.SystemColors.Window
        Me.txtNombre.Location = New System.Drawing.Point(12, 43)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.ReadOnly = True
        Me.txtNombre.Size = New System.Drawing.Size(361, 20)
        Me.txtNombre.TabIndex = 1
        '
        'txtDescCorta
        '
        Me.txtDescCorta.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescCorta.Location = New System.Drawing.Point(12, 144)
        Me.txtDescCorta.Name = "txtDescCorta"
        Me.txtDescCorta.ReadOnly = True
        Me.txtDescCorta.Size = New System.Drawing.Size(361, 20)
        Me.txtDescCorta.TabIndex = 2
        '
        'txtDescLarga
        '
        Me.txtDescLarga.AcceptsTab = True
        Me.txtDescLarga.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescLarga.Location = New System.Drawing.Point(12, 170)
        Me.txtDescLarga.Multiline = True
        Me.txtDescLarga.Name = "txtDescLarga"
        Me.txtDescLarga.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescLarga.Size = New System.Drawing.Size(361, 189)
        Me.txtDescLarga.TabIndex = 3
        '
        'ListClases
        '
        Me.ListClases.FormattingEnabled = True
        Me.ListClases.Location = New System.Drawing.Point(379, 14)
        Me.ListClases.Name = "ListClases"
        Me.ListClases.Size = New System.Drawing.Size(129, 56)
        Me.ListClases.TabIndex = 4
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(194, 14)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(53, 17)
        Me.RadioButton1.TabIndex = 7
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Ingles"
        Me.RadioButton1.UseVisualStyleBackColor = True
        Me.RadioButton1.Visible = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(253, 14)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(63, 17)
        Me.RadioButton2.TabIndex = 7
        Me.RadioButton2.Text = "Español"
        Me.RadioButton2.UseVisualStyleBackColor = True
        Me.RadioButton2.Visible = False
        '
        'ListDatos
        '
        Me.ListDatos.FormattingEnabled = True
        Me.ListDatos.Location = New System.Drawing.Point(13, 69)
        Me.ListDatos.Name = "ListDatos"
        Me.ListDatos.Size = New System.Drawing.Size(360, 69)
        Me.ListDatos.TabIndex = 8
        '
        'cmdGuardar
        '
        Me.cmdGuardar.Location = New System.Drawing.Point(413, 268)
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.Size = New System.Drawing.Size(87, 41)
        Me.cmdGuardar.TabIndex = 9
        Me.cmdGuardar.Text = "Guardar"
        Me.cmdGuardar.UseVisualStyleBackColor = True
        Me.cmdGuardar.Visible = False
        '
        'cmdNew
        '
        Me.cmdNew.Location = New System.Drawing.Point(413, 315)
        Me.cmdNew.Name = "cmdNew"
        Me.cmdNew.Size = New System.Drawing.Size(87, 43)
        Me.cmdNew.TabIndex = 10
        Me.cmdNew.Text = "Nuevo"
        Me.cmdNew.UseVisualStyleBackColor = True
        Me.cmdNew.Visible = False
        '
        'cmdExport
        '
        Me.cmdExport.Location = New System.Drawing.Point(413, 395)
        Me.cmdExport.Name = "cmdExport"
        Me.cmdExport.Size = New System.Drawing.Size(87, 23)
        Me.cmdExport.TabIndex = 11
        Me.cmdExport.Text = "Exportación ->"
        Me.cmdExport.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(201, 381)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(32, 23)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'cmdFiltro
        '
        Me.cmdFiltro.Location = New System.Drawing.Point(12, 395)
        Me.cmdFiltro.Name = "cmdFiltro"
        Me.cmdFiltro.Size = New System.Drawing.Size(87, 23)
        Me.cmdFiltro.TabIndex = 13
        Me.cmdFiltro.Text = "<- Busqueda"
        Me.cmdFiltro.UseVisualStyleBackColor = True
        '
        'txtManual
        '
        Me.txtManual.BackColor = System.Drawing.SystemColors.Window
        Me.txtManual.Location = New System.Drawing.Point(151, 12)
        Me.txtManual.Name = "txtManual"
        Me.txtManual.ReadOnly = True
        Me.txtManual.Size = New System.Drawing.Size(37, 20)
        Me.txtManual.TabIndex = 14
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(103, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Manual"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkXP)
        Me.GroupBox1.Controls.Add(Me.chkDF)
        Me.GroupBox1.Controls.Add(Me.chkF)
        Me.GroupBox1.Controls.Add(Me.chkXM)
        Me.GroupBox1.Controls.Add(Me.chkM)
        Me.GroupBox1.Controls.Add(Me.chkS)
        Me.GroupBox1.Controls.Add(Me.chkV)
        Me.GroupBox1.Location = New System.Drawing.Point(379, 76)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(135, 158)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Componentes"
        '
        'chkXP
        '
        Me.chkXP.AutoSize = True
        Me.chkXP.Location = New System.Drawing.Point(6, 127)
        Me.chkXP.Name = "chkXP"
        Me.chkXP.Size = New System.Drawing.Size(81, 17)
        Me.chkXP.TabIndex = 5
        Me.chkXP.TabStop = False
        Me.chkXP.Text = "Experiencia"
        Me.chkXP.UseMnemonic = False
        Me.chkXP.UseVisualStyleBackColor = True
        '
        'chkDF
        '
        Me.chkDF.AutoSize = True
        Me.chkDF.Location = New System.Drawing.Point(6, 104)
        Me.chkDF.Name = "chkDF"
        Me.chkDF.Size = New System.Drawing.Size(83, 17)
        Me.chkDF.TabIndex = 5
        Me.chkDF.TabStop = False
        Me.chkDF.Text = "Foco Divino"
        Me.chkDF.UseMnemonic = False
        Me.chkDF.UseVisualStyleBackColor = True
        '
        'chkF
        '
        Me.chkF.AutoSize = True
        Me.chkF.Location = New System.Drawing.Point(6, 81)
        Me.chkF.Name = "chkF"
        Me.chkF.Size = New System.Drawing.Size(50, 17)
        Me.chkF.TabIndex = 5
        Me.chkF.TabStop = False
        Me.chkF.Text = "Foco"
        Me.chkF.UseMnemonic = False
        Me.chkF.UseVisualStyleBackColor = True
        '
        'chkXM
        '
        Me.chkXM.AutoSize = True
        Me.chkXM.Location = New System.Drawing.Point(75, 58)
        Me.chkXM.Name = "chkXM"
        Me.chkXM.Size = New System.Drawing.Size(48, 17)
        Me.chkXM.TabIndex = 5
        Me.chkXM.TabStop = False
        Me.chkXM.Text = "Caro"
        Me.chkXM.UseMnemonic = False
        Me.chkXM.UseVisualStyleBackColor = True
        '
        'chkM
        '
        Me.chkM.AutoSize = True
        Me.chkM.Location = New System.Drawing.Point(6, 58)
        Me.chkM.Name = "chkM"
        Me.chkM.Size = New System.Drawing.Size(63, 17)
        Me.chkM.TabIndex = 5
        Me.chkM.TabStop = False
        Me.chkM.Text = "Material"
        Me.chkM.UseMnemonic = False
        Me.chkM.UseVisualStyleBackColor = True
        '
        'chkS
        '
        Me.chkS.AutoSize = True
        Me.chkS.Location = New System.Drawing.Point(6, 35)
        Me.chkS.Name = "chkS"
        Me.chkS.Size = New System.Drawing.Size(70, 17)
        Me.chkS.TabIndex = 5
        Me.chkS.TabStop = False
        Me.chkS.Text = "Somatico"
        Me.chkS.UseMnemonic = False
        Me.chkS.UseVisualStyleBackColor = True
        '
        'chkV
        '
        Me.chkV.AutoSize = True
        Me.chkV.Location = New System.Drawing.Point(6, 12)
        Me.chkV.Name = "chkV"
        Me.chkV.Size = New System.Drawing.Size(56, 17)
        Me.chkV.TabIndex = 5
        Me.chkV.TabStop = False
        Me.chkV.Text = "Verbal"
        Me.chkV.UseMnemonic = False
        Me.chkV.UseVisualStyleBackColor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 10000
        Me.ToolTip1.InitialDelay = 0
        Me.ToolTip1.ReshowDelay = 0
        Me.ToolTip1.ShowAlways = True
        '
        'frmSpell
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(525, 443)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtManual)
        Me.Controls.Add(Me.cmdFiltro)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cmdExport)
        Me.Controls.Add(Me.cmdNew)
        Me.Controls.Add(Me.cmdGuardar)
        Me.Controls.Add(Me.ListDatos)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.ListClases)
        Me.Controls.Add(Me.txtDescLarga)
        Me.Controls.Add(Me.txtDescCorta)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtIdConjuro)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSpell"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consulta de conjuros v0.1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtIdConjuro As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtDescCorta As System.Windows.Forms.TextBox
    Friend WithEvents txtDescLarga As System.Windows.Forms.TextBox
    Friend WithEvents ListClases As System.Windows.Forms.ListBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents ListDatos As System.Windows.Forms.ListBox
    Friend WithEvents cmdGuardar As System.Windows.Forms.Button
    Friend WithEvents cmdNew As System.Windows.Forms.Button
    Friend WithEvents cmdExport As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cmdFiltro As System.Windows.Forms.Button
    Friend WithEvents txtManual As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkXP As System.Windows.Forms.CheckBox
    Friend WithEvents chkDF As System.Windows.Forms.CheckBox
    Friend WithEvents chkF As System.Windows.Forms.CheckBox
    Friend WithEvents chkXM As System.Windows.Forms.CheckBox
    Friend WithEvents chkM As System.Windows.Forms.CheckBox
    Friend WithEvents chkS As System.Windows.Forms.CheckBox
    Friend WithEvents chkV As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

End Class
