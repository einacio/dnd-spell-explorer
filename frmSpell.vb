Option Explicit On
Option Strict On
Public Class frmSpell
    Private p_idConjuroActual As Integer = 0
    Public ReadOnly Property idConjuroActual() As Integer
        Get
            idConjuroActual = p_idConjuroActual
        End Get
    End Property
    Public Sub CargarConjuro(ByVal idConjuro As Integer)
        util.extClearForm(Me)
        p_idConjuroActual = 0
        Using cn As New System.Data.OleDb.OleDbConnection(connStr)
            cn.Open()
            Dim cm As OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader

            cm = New OleDb.OleDbCommand()
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from v_conjuros where idconjuro=@idconjuro"
                .Parameters.AddWithValue("@idconjuro", idConjuro)
                rs = .ExecuteReader(CommandBehavior.SingleRow)
            End With
            If rs.HasRows Then
                p_idConjuroActual = idConjuro
                rs.Read()
                txtIdConjuro.Text = rs.Item("idconjuro").ToString()
                txtNombre.Text = rs("nombre_ing").ToString()
                txtDescCorta.Text = rs("descripcioncorta_ing").ToString()
                txtDescLarga.Text = rs("descripcion_ing").ToString()
                chkV.Checked = CBool(rs("v"))
                chkS.Checked = CBool(rs("s"))
                chkF.Checked = CBool(rs("f"))
                chkDF.Checked = CBool(rs("df"))
                chkXP.Checked = CBool(rs("xp"))
                chkM.Checked = CBool(rs("m"))
                chkXM.Checked = CBool(rs("xm"))
                txtManual.Text = rs("manual").ToString
                ToolTip1.SetToolTip(txtManual, rs("manual_ing").ToString)
                With ListDatos.Items
                    .Add(rs("escuela_ing"))
                    If rs("subescuela_ing").ToString().Trim <> "" Then
                        .Item(0) = .Item(0).ToString & " (" & rs("subescuela_ing").ToString().Trim & ")"
                    End If
                    .Add("Casting Time: " & rs("tiempo_ing").ToString)
                    .Add("Range: " & rs("alcance_ing").ToString)
                    If rs("objetivo_ing").ToString().Trim <> "" Then
                        .Add("Target: " & rs("objetivo_ing").ToString)
                    End If
                    If rs("efecto_ing").ToString().Trim <> "" Then
                        .Add("Effect: " & rs("efecto_ing").ToString)
                    End If
                    If rs("area_ing").ToString().Trim <> "" Then
                        .Add("Area: " & rs("area_ing").ToString)
                    End If
                    .Add("Duration: " & rs("duracion_ing").ToString)
                    .Add("Saving Throw: " & rs("salvacion_ing").ToString)
                    If rs("resistencia_ing").ToString.Trim <> "" Then
                        .Add("Spell Resistance: " & rs("resistencia_ing").ToString)
                    End If
                End With
                rs.Close()

                cm = New OleDb.OleDbCommand()
                With cm
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from v_conjuros_clases where idconjuro=@idconjuro"
                    .Parameters.AddWithValue("@idconjuro", idConjuro)
                    rs = .ExecuteReader(CommandBehavior.SingleResult)
                End With
                While rs.Read()
                    ListClases.Items.Add(rs("clase_ing").ToString() & " " & rs("nivel").ToString())
                End While
                rs.Close()

                cm = New OleDb.OleDbCommand()
                With cm
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = "select * from v_conjuros_descriptores where idconjuro=@idconjuro order by descriptor_ing"
                    .Parameters.AddWithValue("@idconjuro", idConjuro)
                    rs = .ExecuteReader(CommandBehavior.SingleResult)
                End With
                If rs.HasRows Then
                    Dim a As ListBox.ObjectCollection = ListDatos.Items
                    a.Item(0) = a.Item(0).ToString & " ["
                    While rs.Read()
                        a.Item(0) = a.Item(0).ToString & rs("descriptor_ing").ToString() & ", "
                    End While
                    'a.Item(0) = Microsoft.VisualBasic.Left(a.Item(0).ToString, Len(a.Item(0).ToString()) - 1) & "]"
                    a.Item(0) = a.Item(0).ToString().Remove(a.Item(0).ToString.Length - 2) & "]"
                End If
                rs.Close()
            End If
            cn.Close()
        End Using
    End Sub

    Private Sub txtIdConjuro_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIdConjuro.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            e.Handled = True
            If IsNumeric(txtIdConjuro.Text) Then
                CargarConjuro(CInt(txtIdConjuro.Text))
            End If
        End If
    End Sub

    Private Sub chkXM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If chkXM.Checked = True Then
            chkM.Checked = True
        End If
    End Sub

    Private Sub chkM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If chkM.Checked = False Then
            chkXM.Checked = False
        End If
    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        'Using cn As New System.Data.OleDb.OleDbConnection(connStr)
        '    cn.Open()
        '    Dim cm As OleDb.OleDbCommand

        '    cm = New OleDb.OleDbCommand()
        '    cm.Connection = cn
        '    cm.CommandType = CommandType.Text
        '    cm.CommandText = "select max(idconjuro)+1 from conjuros"
        '    cm.ExecuteNonQuery()
        '    cn.Close()
        'End Using
    End Sub

    Public Sub New()

        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub chkNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNew.Click
        extClearForm(Me)
        Using cn As New System.Data.OleDb.OleDbConnection(connStr)
            cn.Open()
            Dim cm As OleDb.OleDbCommand

            cm = New OleDb.OleDbCommand()
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select max(idconjuro)+1 from conjuros"
                txtIdConjuro.Text = .ExecuteScalar.ToString
            End With
            cn.Close()
        End Using
    End Sub

    Private Sub cmdExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExport.Click
        If Not frmExport.Visible Then
            frmExport.Location = New Point(Me.Location.X + Me.Size.Width, Me.Location.Y)
            frmExport.Show()

            cmdExport.Text = "Exportación <-"
        Else
            frmExport.Hide()
            cmdExport.Text = "Exportación ->"
        End If
    End Sub

    Private Sub frmSpell_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        If frmExport.Visible Then
            'If frmExport.Docked Then
            'frmExport.Location = New Point(Me.Location.X + Me.Size.Width, Me.Location.Y)
            frmExport.Left = Me.Left + Me.Width
            frmExport.Top = Me.Top
        End If
        If frmFiltro.Visible Then
            frmFiltro.Left = Me.Left - frmFiltro.Width
            frmFiltro.Top = Me.Top
            'End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim a As System.Net.HttpWebRequest = CType(System.Net.WebRequest.Create("http://localhost/conexionDnd.php"), System.Net.HttpWebRequest)
            a.UserAgent = "DnD/v0.1"
            a.Method = "POST"
            a.ContentType = "application/x-www-form-urlencoded"
            Dim post As String = "uno=blje&dos=trkj"
            Dim encoding As New System.Text.ASCIIEncoding()
            Dim byte1 As Byte() = encoding.GetBytes(post)
            a.ContentLength = byte1.Length
            Dim s As System.IO.Stream = a.GetRequestStream()
            s.Write(byte1, 0, byte1.Length)
            s.Close()

            Dim response As System.Net.HttpWebResponse = CType(a.GetResponse(), System.Net.HttpWebResponse)

            Console.WriteLine("Content length is {0}", response.StatusCode)
            Console.WriteLine("Content type is {0}", response.StatusDescription)

            ' Get the stream associated with the response.
            Dim receiveStream As System.IO.Stream = response.GetResponseStream()

            ' Pipes the stream to a higher level stream reader with the required encoding format. 
            Dim readStream As New System.IO.StreamReader(receiveStream, System.Text.Encoding.UTF8)

            Console.WriteLine("Response stream received.")
            Console.WriteLine(readStream.ReadToEnd())
            response.Close()
            readStream.Close()
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFiltro.Click
        If Not frmFiltro.Visible Then
            frmFiltro.Location = New Point(Me.Location.X - frmFiltro.Size.Width, Me.Location.Y)
            frmFiltro.Show()
            cmdFiltro.Text = "-> Busqueda"
        Else
            frmFiltro.Hide()
            cmdFiltro.Text = "<- Busqueda"
        End If
    End Sub

    Private Sub frmSpell_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Button2_Click(New System.Object, New System.EventArgs)
    End Sub

    Private Sub frmSpell_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub chkV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkV.Click, chkDF.Click, chkF.Click, chkM.Click, chkS.Click, chkXM.Click, chkXP.Click
        CType(sender, CheckBox).Checked = Not CType(sender, CheckBox).Checked
    End Sub

    Private Sub txtManual_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtManual.MouseHover

    End Sub

    Private Sub txtManual_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtManual.TextChanged

    End Sub
End Class
