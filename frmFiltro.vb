Option Strict On
Public Class frmFiltro
    Private Enum eOrdenPresentacion
        ClaseNv = 0
        Alfabetico = 1
        ManClaseNv = 2
        Libre = 3
    End Enum
    Private Structure Escuelas
        Dim idEscuela As Integer
        Dim idSubEscuela As Integer
        'Friend Sub New(ByVal idE As Integer, ByVal idS As Integer)
        '    idEscuela = idE
        '    idSubEscuela = idS
        'End Sub
    End Structure
    Private FiltroClase As Boolean
    Private FiltroNivel As Boolean
    Private FiltroMateriales As Boolean
    Private FiltroManual As Boolean
    Private FiltroTexto As Boolean
    Private FiltroEscuela As Boolean
    Private FiltroDescriptor As Boolean

    Private idsClase As List(Of Integer)
    Private idsManual As List(Of String)
    Private idsEscuelas As List(Of Escuelas)
    Private idsDescriptor As List(Of Integer)

    Private ReadOnly Property ConFiltros() As Boolean
        Get
            ConFiltros = FiltroClase OrElse FiltroNivel OrElse FiltroMateriales OrElse FiltroManual OrElse FiltroTexto OrElse FiltroEscuela OrElse FiltroDescriptor
        End Get
    End Property

    Private Sub frmFiltro_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Label1.BackColor = SystemColors.ActiveCaption
        Label1.ForeColor = SystemColors.ActiveCaptionText
    End Sub

    Private Sub frmFiltro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Height = frmSpell.Height
        Me.Owner = frmSpell
        cmbOrden.SelectedIndex = 0
        '        CargarListaCompletaClaseNv()
        CargarClases()
        CargarManuales()
        CargarEscuelas()
        CargarDescriptor()
    End Sub

    Private Sub frmFiltro_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        Label1.BackColor = SystemColors.InactiveCaption
        Label1.ForeColor = SystemColors.InactiveCaptionText
    End Sub

    Private Sub CargarListaCompletaClaseNv()
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            cn.Open()
            cm = New OleDb.OleDbCommand
            With cm
                .Connection = cn
                .CommandType = CommandType.TableDirect
                .CommandText = "v_listaComp"
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            CargarLista(rs)
            cn.Close()
        End Using
    End Sub

    Private Sub CargarListaCompletaAlfa()
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            cn.Open()
            cm = New OleDb.OleDbCommand
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select distinct idconjuro, nombre_ing from conjuros order by nombre_Ing"
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            CargarLista(rs)
            cn.Close()
        End Using
    End Sub

    Private Sub CargarListaCompletaManClaseNv()
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            cn.Open()
            cm = New OleDb.OleDbCommand
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from v_listaComp order by manual, clase_ing, nivel, nombre_ing"
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            CargarLista(rs)
            cn.Close()
        End Using
    End Sub


    Private Sub CargarLista(ByVal rs As OleDb.OleDbDataReader)
        Dim tipo As eOrdenPresentacion = CType(cmbOrden.SelectedIndex, eOrdenPresentacion)
        TVRes.Nodes.Clear()
        Select Case tipo
            Case eOrdenPresentacion.ClaseNv
                'campos requeridos= idconjuro, nombre_ing, clase_ing, nivel
                Dim clase As String = ""
                Dim nivel As Integer = 0
                Dim nodoActual As TreeNode = New TreeNode
                Dim echoNode As TreeNode
                While rs.Read
                    If clase <> rs("clase_ing").ToString Then
                        clase = rs("clase_ing").ToString
                        TVRes.Nodes.Add(clase, clase)
                        nivel = 0
                    End If
                    If nivel < CInt(rs("nivel")) Then
                        nivel = CInt(rs("nivel"))
                        nodoActual = TVRes.Nodes.Item(clase).Nodes.Add(clase & "NV" & nivel.ToString, "Nv. " & nivel.ToString)
                    End If
                    echoNode = nodoActual.Nodes.Add(rs("nombre_ing").ToString)
                    echoNode.Tag = rs("idconjuro")
                End While
            Case eOrdenPresentacion.Alfabetico
                'campos requeridos= idconjuro, nombre_ing
                Dim letra As String = ""
                Dim nodoActual As TreeNode = New TreeNode
                Dim echoNode As TreeNode
                While rs.Read
                    If letra <> rs("nombre_ing").ToString().Substring(0, 1).ToUpper Then
                        letra = rs("nombre_ing").ToString().Substring(0, 1).ToUpper
                        nodoActual = TVRes.Nodes.Add(letra, letra)
                    End If
                    echoNode = nodoActual.Nodes.Add(rs("nombre_ing").ToString)
                    echoNode.Tag = rs("idconjuro")
                End While
            Case eOrdenPresentacion.ManClaseNv
                'campos requeridos=idconjuro, clase_ing, nivel, nombre_ing, manual
                Dim clase As String = ""
                Dim manual As String = ""
                Dim nivel As Integer = 0
                Dim nodoActual As TreeNode = New TreeNode
                Dim nodoClase As TreeNode = New TreeNode
                Dim echoNode As TreeNode
                While rs.Read
                    If manual <> rs("manual").ToString Then
                        manual = rs("manual").ToString
                        TVRes.Nodes.Add(manual, rs("manual_ing").ToString & " (" & manual & ")")
                        clase = ""
                    End If
                    If clase <> rs("clase_ing").ToString Then
                        clase = rs("clase_ing").ToString
                        nodoClase = TVRes.Nodes(manual).Nodes.Add(clase, clase)
                        nivel = 0
                    End If
                    If nivel < CInt(rs("nivel")) Then
                        nivel = CInt(rs("nivel"))
                        nodoActual = nodoClase.Nodes.Add(clase & "NV" & nivel.ToString, "Nv. " & nivel.ToString)
                    End If
                    echoNode = nodoActual.Nodes.Add(rs("nombre_ing").ToString)
                    echoNode.Tag = rs("idconjuro")
                End While
            Case eOrdenPresentacion.Libre
                Dim echoNode As TreeNode
                While rs.Read
                    echoNode = TVRes.Nodes.Add(rs("nombre_ing").ToString)
                    echoNode.Tag = rs("idconjuro")
                End While
        End Select
    End Sub

    Private Sub TVRes_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TVRes.AfterSelect
        If Not IsNothing(e.Node.Tag) Then
            frmSpell.CargarConjuro(CInt(e.Node.Tag))
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOrden.SelectedIndexChanged
        If Not ConFiltros Then
            Select Case CType(sender, ComboBox).SelectedIndex
                Case 0
                    CargarListaCompletaClaseNv()
                Case 1, 3
                    CargarListaCompletaAlfa()
                Case 2
                    CargarListaCompletaManClaseNv()
            End Select
        Else
            CargarListaFiltrada()
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked, lnkClases.LinkClicked, lnkMan.LinkClicked, lnkMat.LinkClicked, lnkNivel.LinkClicked, lnkTexto.LinkClicked, lnkEscuela.LinkClicked, lnkDescriptor.LinkClicked
        Dim i As Integer
        Dim GB As GroupBox = CType(CType(sender, LinkLabel).Parent, GroupBox)

        Dim max As Integer
        max = GB.MaximumSize.Height
        If max = 0 Then max = 200
        If GB.Name = "GroupBox1" Then
            GB.AutoSize = False
            max = 240
            Dim c As Control
            For Each c In GB.Controls
                If c.MinimumSize.Height <> 0 Then
                    c.Height = c.MinimumSize.Height
                End If
            Next
        Else
            GroupBox1.AutoSize = True
        End If

        If GB.Height = GB.MinimumSize.Height Then
            GB.BringToFront()
            For i = GB.MinimumSize.Height To max Step 10
                System.Threading.Thread.Sleep(10)
                'i += 10
                GB.Height = i
                Application.DoEvents()
            Next
            'GroupBox1.Height = 50
            'GroupBox1.Height = 100
            'GroupBox1.Height = 150
            'GroupBox1.Height = 200
        Else
            GB.Height = GB.MinimumSize.Height
        End If
    End Sub

    Private Sub CargarClases()
        idsClase = New List(Of Integer)
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand = New OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select idclase,clase_ing from clases order by clase_ing"
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            While rs.Read
                lstClases.Items.Add(rs("clase_ing"))
                idsClase.Add(CInt(rs("idclase")))
            End While
            cn.Close()
        End Using
    End Sub

    Private Sub CargarManuales()
        idsManual = New List(Of String)
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand = New OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select idmanual,manual_ing from manuales order by manual_ing"
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            While rs.Read
                lstManuales.Items.Add(rs.Item("manual_ing"))
                idsManual.Add(rs.Item("idmanual").ToString)
            End While
            cn.Close()
        End Using
    End Sub

    Private Sub CargarEscuelas()
        idsEscuelas = New List(Of Escuelas)
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand = New OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            Dim escuela As Escuelas
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select e.idescuela, s.idsubescuela, e.escuela_ing, s.subescuela_ing from escuelas e, subescuelas s where e.idescuela=s.idescuela order by e.escuela_ing,s.subescuela_ing"
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            While rs.Read
                lstEscuela.Items.Add(rs.Item("escuela_ing").ToString & " (" & IIf(CInt(rs("idsubescuela")) = 0, "Todos", rs("subescuela_ing")).ToString & ")")
                escuela.idEscuela = CInt(rs("idescuela"))
                escuela.idSubEscuela = CInt(rs("idsubescuela"))
                idsEscuelas.Add(escuela)
            End While
            cn.Close()
        End Using
    End Sub

    Private Sub CargarDescriptor()
        idsDescriptor = New List(Of Integer)
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand = New OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = "select * from descriptores order by descriptor_ing"
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            While rs.Read
                lstDescriptor.Items.Add(rs.Item("descriptor_ing").ToString)
                idsDescriptor.Add(CInt(rs("iddescriptor")))
            End While
            cn.Close()
        End Using
    End Sub

    Private Sub CargarListaFiltrada()
        If Not ConFiltros Then
            Exit Sub
        End If

        Dim tipo As eOrdenPresentacion = CType(cmbOrden.SelectedIndex, eOrdenPresentacion)
        Dim strFROM As String = "select v_listacomp.* from v_listaComp "
        Dim strWHERE As String = "where "
        Dim temp As String
        Dim i As Integer
        TVRes.Nodes.Clear()
        If FiltroTexto Then
            strFROM &= ", conjuros "
            strWHERE &= "v_listacomp.idconjuro=conjuros.idconjuro and "
            If txtTextoAND.TextLength > 0 Then
                temp = "(instr(@@,'" & txtTextoAND.Text.Replace(" ", "') and instr(@@,'") & "'))"
                strWHERE &= "(" & temp.Replace("@@", "conjuros.nombre_ing") & " or " & temp.Replace("@@", "efecto_ing") & " or " & temp.Replace("@@", "descripcioncorta_ing") & " or " & temp.Replace("@@", "descripcion_ing") & ") and "
            End If
            If txtTextoOR.TextLength > 0 Then
                temp = "(instr(@@,'" & txtTextoOR.Text.Replace(" ", "') or instr(@@,'") & "'))"
                strWHERE &= "(" & temp.Replace("@@", "conjuros.nombre_ing") & " or " & temp.Replace("@@", "efecto_ing") & " or " & temp.Replace("@@", "descripcioncorta_ing") & " or " & temp.Replace("@@", "descripcion_ing") & ") and "
            End If
        End If
        If FiltroClase Then
            temp = ""
            For i = 0 To lstClases.CheckedIndices.Count - 1
                temp &= idsClase(lstClases.CheckedIndices.Item(i)).ToString & ","
            Next
            strWHERE &= "idclase in(" & temp.Substring(0, temp.Length - 1) & ") and "
        End If

        If FiltroNivel Then
            temp = ""
            For i = 0 To lstNivel.CheckedIndices.Count - 1
                temp &= lstNivel.CheckedIndices.Item(i).ToString & ","
            Next
            strWHERE &= "nivel in(" & temp.Substring(0, temp.Length - 1) & ") and "
        End If

        If FiltroMateriales Then
            If Not strFROM.Contains(", conjuros") Then
                strFROM &= ", conjuros "
                strWHERE &= "v_listacomp.idconjuro=conjuros.idconjuro and "
            End If
            If chkV.Checked Then
                strWHERE &= "V=true and "
            End If
            If chkS.Checked Then
                strWHERE &= "S=true and "
            End If
            If chkM.Checked Then
                strWHERE &= "M=true and "
            End If
            If chkXM.Checked Then
                strWHERE &= "XM=true and "
            End If
            If chkDF.Checked Then
                strWHERE &= "DF=true and "
            End If
            If chkF.Checked Then
                strWHERE &= "F=true and "
            End If
            If chkXP.Checked Then
                strWHERE &= "XP=true and "
            End If
        End If

        If FiltroManual Then
            temp = ""
            For i = 0 To lstManuales.CheckedIndices.Count - 1
                temp &= "'" & idsManual(lstManuales.CheckedIndices.Item(i)) & "',"
            Next
            strWHERE &= "manual in(" & temp.Substring(0, temp.Length - 1) & ") and "
        End If

        If FiltroEscuela Then
            If Not strFROM.Contains(", conjuros") Then
                strFROM &= ", conjuros "
                strWHERE &= "v_listacomp.idconjuro=conjuros.idconjuro and "
            End If
            temp = ""
            For i = 0 To lstEscuela.CheckedIndices.Count - 1
                temp &= "(idescuela =" & idsEscuelas(lstEscuela.CheckedIndices.Item(i)).idEscuela.ToString
                If idsEscuelas(lstEscuela.CheckedIndices.Item(i)).idSubEscuela <> 0 Then
                    temp &= " and idsubescuela =" & idsEscuelas(lstEscuela.CheckedIndices.Item(i)).idSubEscuela.ToString
                End If
                temp &= ") or "
            Next
            strWHERE &= "(" & temp.Substring(0, temp.Length - 4) & ") and "
        End If

        If FiltroDescriptor Then
            If Not strFROM.Contains(", conjuros") Then
                strFROM &= ", conjuros "
                strWHERE &= "v_listacomp.idconjuro=conjuros.idconjuro and "
            End If
            temp = ""
            For i = 0 To lstDescriptor.CheckedIndices.Count - 1
                temp &= idsDescriptor(lstDescriptor.CheckedIndices.Item(i)) & ","
            Next
            strWHERE &= "conjuros.idconjuro in (select idconjuro from conjuros_descriptores where iddescriptor in(" & temp.Substring(0, temp.Length - 1) & ")) and "
        End If


        strWHERE = strWHERE.Substring(0, strWHERE.Length - 4)

        Select Case tipo
            Case eOrdenPresentacion.Alfabetico, eOrdenPresentacion.Libre
                strFROM = strFROM.Replace("v_listacomp.*", "distinct v_listacomp.idconjuro, v_listacomp.nombre_ing")
                strWHERE &= "order by v_listacomp.nombre_ing"
            Case eOrdenPresentacion.ClaseNv
                strWHERE &= "order by v_listacomp.clase_ing, nivel"
            Case eOrdenPresentacion.ManClaseNv
                strWHERE &= "order by v_listacomp.manual,v_listacomp.clase_ing, nivel"
        End Select
        Using cn As New OleDb.OleDbConnection(connStr)
            Dim cm As OleDb.OleDbCommand = New OleDb.OleDbCommand
            Dim rs As OleDb.OleDbDataReader
            cn.Open()
            With cm
                .Connection = cn
                .CommandType = CommandType.Text
                .CommandText = strFROM & strWHERE
                rs = .ExecuteReader(CommandBehavior.SingleResult)
            End With
            CargarLista(rs)
            cn.Close()
        End Using
        Debug.Print(strFROM & strWHERE)
    End Sub

    Private Sub lstClases_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lstClases.ItemCheck
        If e.NewValue = CheckState.Checked Then
            FiltroClase = True
        Else
            'el evento se activa cuando se hace click, 
            'antes de cambiar el check, asi que nos fijamos 
            'si el que se deschequea es el ultimo
            If lstClases.CheckedItems.Count = 1 Then
                FiltroClase = False
            End If
        End If
    End Sub

    Private Sub lstNivel_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lstNivel.ItemCheck
        If e.NewValue = CheckState.Checked Then
            FiltroNivel = True
        Else
            'el evento se activa cuando se hace click, 
            'antes de cambiar el check, asi que nos fijamos 
            'si el que se deschequea es el ultimo
            If lstNivel.CheckedItems.Count = 1 Then
                FiltroNivel = False
            End If
        End If
    End Sub

    Private Sub cmdFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFiltrar.Click
        CargarListaFiltrada()
        GroupBox1.AutoSize = False
        GroupBox1.Height = GroupBox1.MinimumSize.Height
    End Sub

    Private Sub chkMateriales_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkV.CheckedChanged, chkDF.CheckedChanged, chkF.CheckedChanged, chkM.CheckedChanged, chkS.CheckedChanged, chkXM.CheckedChanged, chkXP.CheckedChanged
        If CType(sender, CheckBox).Checked Then
            FiltroMateriales = True
        Else
            If Not (chkV.Checked OrElse chkS.Checked OrElse chkM.Checked OrElse chkXM.Checked OrElse chkF.Checked OrElse chkDF.Checked OrElse chkXP.Checked) Then
                FiltroMateriales = False
            End If
        End If
    End Sub

    Private Sub lstManuales_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lstManuales.ItemCheck
        If e.NewValue = CheckState.Checked Then
            FiltroManual = True
        Else
            'el evento se activa cuando se hace click, 
            'antes de cambiar el check, asi que nos fijamos 
            'si el que se deschequea es el ultimo
            If lstManuales.CheckedItems.Count = 1 Then
                FiltroManual = False
            End If
        End If
    End Sub

    Private Sub txtTextoAND_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTextoAND.LostFocus, txtTextoOR.LostFocus
        CType(sender, TextBox).Text = CType(sender, TextBox).Text.Trim()
        txtTexto_TextChanged(New Object, New System.EventArgs)
    End Sub

    Private Sub txtTexto_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtTextoAND.MouseDoubleClick, txtTextoOR.MouseDoubleClick
        CType(sender, TextBox).SelectAll()
    End Sub

    Private Sub txtTexto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTextoAND.TextChanged, txtTextoOR.TextChanged
        If txtTextoAND.TextLength > 0 OrElse txtTextoOR.TextLength > 0 Then
            FiltroTexto = True
        Else
            FiltroTexto = False
        End If
    End Sub

    Private Sub ExpandirTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExpandirTodoToolStripMenuItem.Click
        TVRes.ExpandAll()
    End Sub

    Private Sub ContraerTodoToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContraerTodoToolStripMenuItem.Click
        TVRes.CollapseAll()
    End Sub

    Private Sub lstEscuela_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lstEscuela.ItemCheck
        If e.NewValue = CheckState.Checked Then
            FiltroEscuela = True
        Else
            'el evento se activa cuando se hace click, 
            'antes de cambiar el check, asi que nos fijamos 
            'si el que se deschequea es el ultimo
            If lstEscuela.CheckedItems.Count = 1 Then
                FiltroEscuela = False
            End If
        End If
    End Sub

    Private Sub lstDescriptor_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lstDescriptor.ItemCheck
        If e.NewValue = CheckState.Checked Then
            FiltroDescriptor = True
        Else
            'el evento se activa cuando se hace click, 
            'antes de cambiar el check, asi que nos fijamos 
            'si el que se deschequea es el ultimo
            If lstDescriptor.CheckedItems.Count = 1 Then
                FiltroDescriptor = False
            End If
        End If
    End Sub
End Class